package com.example.secutygomis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecutyGomisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecutyGomisApplication.class, args);
	}

}
